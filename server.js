var http = require("http"),
    request = require("request-promise-native"),
    baseURL = "https://api.chucknorris.io";

async function callAPI(url){
    return request(url).then((result)=>{return result})
}

console.log("Started server");

var server = http.createServer(async function(req,res){
    var url = baseURL + req.url;
    console.log(url)
    try{
        res.writeHead("200",{"Content-Type":"application/json","Access-Control-Allow-Origin":"*"});
        var output = await callAPI(url);
        res.end(output);
    }catch(error){
        console.log("Error - " + error);
        res.writeHead("500");
        res.end("Internal 500 server error")
    }
});
server.listen(8085);